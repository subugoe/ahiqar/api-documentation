# Ahiqar API documentation

This repo holds the documentation for the [TextAPI](https://subugoe.pages.gwdg.de/emo/text-api/) and
AnnotationAPI implementation of the [Ahiqar project](https://www.sub.uni-goettingen.de/en/projects-research/project-details/projekt/ahiqar/).

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.
See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* [`hugo`](https://gohugo.io/getting-started/installing/)

### Installing

#### Building locally

To work locally with this project, you'll have to follow the steps below:

1. Clone or download this project
2. Switch to the project's directory and type `hugo server` to preview the page
3. Add content
4. Generate the website: `hugo` (optional)

Read more at Hugo's [documentation](https://gohugo.io/overview/introduction/).

##### Preview your site

If you clone or download this project to your local computer and run `hugo server`,
your site can be accessed under `localhost:1313/hugo/`.

The theme used is adapted from <http://themes.gohugo.io/beautifulhugo/>.

## Deployment

This project's static Pages are built by GitLab CI, following the steps defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).
The page is built after each commit to the `master` branch and deployed to <https://subugoe.pages.gwdg.de/ahiqar/api-documentation/>.
Since this repository is still protected, only logged in members of the `subugoe` can see the GitLab page.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.gwdg.de/subugoe/ahiqar/api-documentation/-/tags).

## Authors

* **Michelle Weidling** - *Initial work* - [mrodzis](https://gitlab.gwdg.de/users/mrodzis)

See also the list of [contributors](https://gitlab.gwdg.de/subugoe/ahiqar/api-documentation/-/graphs/master) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* This repo heavily relies on the [docs for the TextAPI](https://subugoe.pages.gwdg.de/emo/text-api/) so kudos to them!
