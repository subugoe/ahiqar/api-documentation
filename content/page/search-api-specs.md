---
title: SearchAPI Specification
version: 0.1.0
---

# SearchAPI for the Ahiqar Project

## Preliminaries

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD",
"SHOULD NOT", "RECOMMENDED", "NOT RECOMMENDED", "MAY", and "OPTIONAL" in this
document are to be interpreted as described in [BCP 14](https://tools.ietf.org/html/bcp14),
[RFC2119](https://tools.ietf.org/html/rfc2119) and
[RFC8174](https://tools.ietf.org/html/rfc8174) when, and only when, they appear
in all capitals, as shown here.

This document is licensed under [CC-BY-ND-4.0](https://creativecommons.org/licenses/by-nd/4.0/legalcode) ([via SPDX](https://spdx.org/licenses/CC-BY-ND-4.0.html)).

The style of this documentation is adapted from [OpenAPI 3.0.2](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.2.md)
specification.

## Status of this Document

The current version of this document is `1.0.0`.

The version number will be applied according to [Semantic Versioning 2.0.0](https://semver.org/spec/v2.0.0.html).

## Definitions
### Cardinalities

| Cardinality | Description | REQUIRED |
|----|----|----|
| 1 | exactly one | yes |
| + | one or more | yes |
| ? | zero or one | no |
| * | zero or more | no |

### Data Types

| Type | Description |
|----|----|
| \[…\] | array |
| string | a sequence of characters, MAY be empty |
| URI | a valid URI, see [URI Syntax](#uri-syntax) |
| int | a non-negative Integer value |

#### URI Syntax

The URI Syntax is used according to the one described at [IIIF](https://iiif.io/api/image/2.1/#uri-syntax).


## About this SearchAPI

The used query syntax and objects are prepared alike [Elasticsearch\`s Search API](https://www.elastic.co/guide/en/elasticsearch/reference/current/search-search.html) (v7.13), but
not completely similar.

This API is a standalone development especially for the Ahiqar project.

## Endpoint

`https://ahikar.sub.uni-goettingen.de/api/search`

## Search Request
`POST` the following object to the endpoint to execute a search.

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| query | 1 | [Simple Query String Object](#simple-query-string-object) | main object for setting up the query |
| from | 1 | int | zero-based index of start item in response |
| size | 1 | int | number of hits to return |

### Simple Query String Object
This object is specified according to the [Elasticsearch Query DSL](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-simple-query-string-query.html).

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| simple_query_string | 1 | [Query String Object](#query-string-object) | main query object |


#### Query String Object
| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| query | 1 | string | search query see [query string rules](#query-string-rules) and [Lucene syntax](https://lucene.apache.org/core/8_9_0/queryparser/org/apache/lucene/queryparser/classic/package-summary.html#package.description) |

##### Query String Rules
* An empty query string is not allowed.
* Wildcard only seach is not allowed.
* Multiple terms are separated by whitespace.
* A whitespace character as term deliminator is equal to ` AND ` by the implementation.
* Leading wildcard is possible.

### Sample Request
```json
{
  "query": {
    "simple_query_string": {
      "query": "term termina*"
    }
  },
  "from": 0,
  "size": 10
}
```

## Search Response

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| hits | 1 | \[[Hits Object](#hits-object)\] | search result hits |
| took | 1 | int | time in ms search execution took on back end side |

### Hits Object

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| total | 1 | [Total Object](#total-object) | properties of the hit list |
| hits | 1 | \[[Hits Array](#hits-array)\] | sequence of hits (ordered by score) |

#### Total Object

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| value | 1 | int | number of total hits (in this scope: pages) |
| manifests | 1 | int | total number of distinct manifests in hits |
| matches | 1 | int | total number of matches (equal to the sum of all values from `occurrencesOnPage`) |
| relation | 1 | string | `eq` (from Elasticsearch), means that there are exactly this number of total hits available |

### Hits Array
This describes a single object from the `hits` array that MUST be present but MAY be empty.

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| item | 1 | URI | path to the item context, w/o base uri |
| label | 1 | string | label of the manifest [see TextAPI](../text-api-specs/#manifest-object) |
| n | 1 | string | page number as in `tei:pb/@n` [see TextAPI](../text-api-specs/#item-object) |
| matches | 1 | [ [match object](#match-object) ] | array of matched strings according to the search terms (useful to present matches when using wildcards). Cannot be empty by definition. |

#### Match Object

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| term | 1 | string | exact matched word or phrase |
| occurrencesOnPage | 1 | number | number of occurrences of this exact match in scope (page) |


### Sample Response

```json
{
  "hits": {
    "total": {
      "value": 2,
      "relation": "eq",
      "manifests": 2,
      "matches": 5
    },
    "hits" : [ {
      "item": "/api/textapi/ahikar/syriac/3r678-186v/latest/item.json",
      "label": "Cod. Arab. 236 Copenhagen",
      "n": "123v",
      "matches": [
        {
          "term": "term",
          "occurrencesOnPage": 3
        },
        {
          "term": "terminal",
          "occurrencesOnPage": 1
        }
      ]
    }, {
      "item": "/api/textapi/ahikar/syriac/3r678-126r/latest/item.json",
      "label": "Cod. Arab. 236 Copenhagen",
      "n": "126r",
      "matches": [ 
        {
          "term": "terminal",
          "occurrencesOnPage": 1
        }]
    } ]
  },
  "took": 678
}
```
