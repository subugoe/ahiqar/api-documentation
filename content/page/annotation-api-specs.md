---
title: AnnotationAPI Specification
version: 5.0.0
---

## Preliminaries

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD",
"SHOULD NOT", "RECOMMENDED", "NOT RECOMMENDED", "MAY", and "OPTIONAL" in this
document are to be interpreted as described in [BCP 14](https://tools.ietf.org/html/bcp14),
[RFC2119](https://tools.ietf.org/html/rfc2119) and
[RFC8174](https://tools.ietf.org/html/rfc8174) when, and only when, they appear
in all capitals, as shown here.

This document is licensed under [CC-BY-ND-4.0](https://creativecommons.org/licenses/by-nd/4.0/legalcode) ([via SPDX](https://spdx.org/licenses/CC-BY-ND-4.0.html)).

The style of this documentation is adapted from [OpenAPI 3.0.2](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.2.md)
specification.

## Status of this Document

The current version of this document is `5.0.0`.

The version number will be applied according to [Semantic Versioning 2.0.0](https://semver.org/spec/v2.0.0.html).

## Definitions

### W3C Annotations

The AnnotationAPI is an implementation of the [W3C Annotation Model](https://www.w3.org/TR/annotation-model/).
This means that we provide an [Annotation Collection](https://www.w3.org/TR/annotation-model/#annotation-collection) per resource and [Annotation Pages](https://www.w3.org/TR/annotation-model/#annotation-page) for each Annotation Collection.

The W3C specs, however, don't assume hierarchical/nested resources as we have in the IIIF derived TextAPI model, which states in the case of Ahiqar that we have a collection of texts ([Collection Object]({{< ref "text-api-specs/#collection-object" >}})), single manuscripts ([Manifest Object]({{< ref "text-api-specs/#manifest-object" >}})) and pages within that manuscript ([Item Object]({{< ref "text-api-specs/#item-object" >}})).

In order to bring these two approaches together, all Objects of the TextAPI (Collection/Manifest/Item) provide an endpoint for delivering an Annotation Collection.
These list the next level items (Collection → Manifest, Manifest → Item, Item → Item) as Annotation Pages including all annotations on that level.
Since Collection Objects are on the top of the hierarchy there aren't any Annotation Pages provided for them.

Additions to the W3C model are marked with an `x-`.

### Cardinalities

| Cardinality | Description | REQUIRED |
|----|----|----|
| 1 | exactly one | yes |
| + | one or more | yes |
| ? | zero or one | no |
| * | zero or more | no |

### Data Types

| Type | Description |
|----|----|
| \[…\] | array |
| string | a sequence of characters, MAY be empty |
| URI | a valid URI, see [URI Syntax](#uri-syntax) |
| MIME type | a valid MIME type according to IANA. See [MDN](https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types) |
| iso639-3 | alpha-3 language code according to [ISO 639-3](https://iso639-3.sil.org). In cases of alternative codes the usage of `bibliographic (B)` is REQUIRED. |
| int | a non-negative Integer value |

### URI Syntax

The URI Syntax is used according to the one described at [IIIF](https://iiif.io/api/image/2.1/#uri-syntax).

## Delivery Service / Available Endpoints

The AnnotationAPI delivery service is the description of endpoints.
All endpoints use the `https` protocol.

### Annotations for Collection Objects

`https://ahiqar.uni-goettingen.de/api/textapi/ahikar/{collection}/annotationCollection.json`

Returns an [Annotation Collection](#annotation-collection) for the given collection.

### Annotations for Manifest Objects

`https://ahiqar.uni-goettingen.de/api/textapi/ahikar/{collection}/{manifest}/annotationCollection.json`

Returns an [Annotation Collection](#annotation-collection) for the given manifest.

---

`https://ahiqar.uni-goettingen.de/api/textapi/ahikar/{collection}/{manifest}/annotationPage.json`

Returns an [Annotation Page](#annotation-page) for the given manifest.

### Annotations for Item Objects

`https://ahiqar.uni-goettingen.de/api/textapi/ahikar/{collection}/{manifest}/{item}/{revision}/annotationCollection.json`

Returns an [Annotation Collection](#annotation-collection) for the given item in the given revision.

---

`https://ahiqar.uni-goettingen.de/api/textapi/ahikar/{collection}/{manifest}/{item}/{revision}/annotationPage.json`

Returns an [Annotation Page](#annotation-page) for the given item.

## Schema

### Annotation Collection

An Annotation Collection presents an overview of the Annotation Pages provided for a given resource.

In case of Ahiqar we deliver Annotation Collections for [Collection](#annotations-for-collection-objects), [Manifest](#annotations-for-manifest-objects), and [Item Objects](#annotations-for-item-objects) at said endpoints.

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| @context | 1 | URI | MUST be `http://www.w3.org/ns/anno.jsonld` |
| id | 1 | URI | a unique identifier for the Annotation Collection in the form of  `http://ahiqar.uni-goettingen.de/ns/annotations/annotationCollection/{collection-URI}` |
| type | 1 | string | MUST be `AnnotationCollection` |
| label | 1 | string | a descriptive title for the Annotation Collection |
| x-creator | 1 | [string] | a string or sequence of strings stating the creator(s) of the resource to which the Annotation Collection belongs to. this information is taken from `tei:editor` in the TEI header of the resource |
| first | 1 | URI | an URI pointing to the API endpoint of the first Annotation Page of this Collection |
| last | 1 | URI | an URI pointing to the API endpoint of the last Annotation Page of this Collection. identical to `first` if the collection has only one Annotation Page |

### Annotation Page

An Annotation Page lists all annotations that occur on them while linking to the previous and next Annotation Pages within an Annotation Collection.

In case of Ahiqar we deliver Annotation Collections for [Collection](#annotations-for-collection-objects), [Manifest](#annotations-for-manifest-objects), and [Item Objects](#annotations-for-item-objects) at said endpoints.

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| @context | 1 | URI | MUST be `http://www.w3.org/ns/anno.jsonld` |
| id | 1 | URI | a unique identifier for the Annotation Collection in the form of  `http://ahiqar.uni-goettingen.de/ns/annotations/annotationCollection/{collection-URI}/{manifest-URI}` |
| type | 1  | string | MUST be `AnnotationPage` |
| partOf | 1 | \[[PartOf Object](#partof-object)\] | information about the Annotation Collection the pPage is part of |
| next | 1 | URI | an URI pointing to the API endpoint of the next Annotation Page of this Annotation Collection. MAY be `null` if there is no following Page |
| prev | 1 | URI | an URI pointing to the API endpoint of the previous Annotation Page of this Annotation Collection. MAY be `null` if there is no previous Page |
| items | 1 | \[[Annotation Item Object](#annotation-item-object)\] | a sequence of annotations |

---

### PartOf Object

Represents a relation between an Annotation Page and an Annotation Collection.

Contains information about the Page's parent Annotation Collection.

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| id | 1 | URI | a unique identifier for the Annotation Collection in the form of  `http://ahiqar.uni-goettingen.de/ns/annotations/annotationCollection/{collection-URI}` |
| label | 1 | string | a descriptive title for the Annotation Collection |

### Annotation Item Object

A sequence of annotations belonging to an Annotation Page.

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| body | 1 | \[[Body Object](#body-object)\] | the content of the annotation |
| target | 1 | \[[Target Object](#target-object)\] | information about the target of the annotation |
| type | 1 | string | MUST be `Annotation`|
| id | 1 | URI | a unique identifier for the annotation in the form of  `http://ahiqar.uni-goettingen.de/ns/annotations/{resource-URI}/{node-ID}`. `{node-ID}` refers to the ID of the TEI node from which the annotation has been extracted |

### Body Object

The content of the annotation.

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| type | 1 | string | MUST be `TextualBody` |
| value | 1 | string | the content of the annotation |
| format | 1 | MIME type | MUST be `text/plain` since we only provide a string as the annotation content |
| x-content-type | 1 | string | the semantic type of the annotation. MUST be one of the following: `Person`, `Place`, `Motif`, `Editorial Comment`, `Reference`, `Variant`. |

#### Notes

When using HTML within `value` the set of allowed elements is limited to the following:

- p
- span
- a

Besides the `href` attribute for `html:a` no further attribute SHOULD be used.

### Target Object

Provides information about the target node or text the annotation refers to.

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| selector | 1 | [CssSelector](#selector-object-cssselector) | a selector pointing to the target |
| format | 1 | MIME type | equals `text/xml` in any case since the annotations always refer to TEI documents |
| language | 1 | iso639-3 | the language of the target node's text or the target text |
| source | 1 | IRI | a URL to the resource to which the annotation points to |

### Selector Object: CssSelector

Selecting the target in a flexible way. Currently TIDO allows for a single CssSelector or a range selector including CssSelectors.

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| type | 1 | string | `CssSelector` |
| value | 1 | string | target selector as a valid css selector. Mainly [class selectors](https://www.w3.org/TR/2018/REC-selectors-3-20181106/#class-html) or [id selectors](https://www.w3.org/TR/2018/REC-selectors-3-20181106/#id-selectors). |

## Appendix

### Class Diagram

![UML class diagram](http://yuml.me/ddaeca73.svg)

```yuml
// Ahiqar AnnotationAPI
// ------------------

// classes
[Annotation Collection|entrypoint|- id 1; -type 1; -label 1; -creator 1; -first 1; -last 1 {bg:yellow}]

[Annotation Page|entrypoint| -@context 1; -id 1; -partOf 1; -next 1; -prev 1; -items 1 {bg:yellow}]

// objects
[PartOf|-id 1; -label 1]

[Annotation Item| -body 1; -@context 1; -target 1; -type 1; -id 1]

[Target| -format 1; -language 1; -source 1; -selector 1]

[Body| -type 1; -value 1; -format 1; -x-content-type 1]

[Selector Object| -type 1; -value 1]

// imports
[Annotation Page]-[PartOf]
[Annotation Page]-[Annotation Item]

[Annotation Item]-[Target]
[Annotation Item]-[Body]

[Target]-[Selector Object]
```

### Examples

- <https://ahiqar.uni-goettingen.de/api/annotations/ahikar/syriac/annotationCollection.json>
- <https://ahiqar.uni-goettingen.de/api/annotations/ahikar/syriac/3rck1/annotationPage.json>
- <https://ahiqar.uni-goettingen.de/api/annotations/ahikar/arabic-karshuni/3r178/140/annotationPage.json>
- <https://ahiqar.uni-goettingen.de/api/annotations/ahikar/arabic-karshuni/3r178/151/annotationPage.json>
