---
title: TextAPI Specification
version: 1.3.0
---

## Preliminaries

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD",
"SHOULD NOT", "RECOMMENDED", "NOT RECOMMENDED", "MAY", and "OPTIONAL" in this
document are to be interpreted as described in [BCP 14](https://tools.ietf.org/html/bcp14),
[RFC2119](https://tools.ietf.org/html/rfc2119) and
[RFC8174](https://tools.ietf.org/html/rfc8174) when, and only when, they appear
in all capitals, as shown here.

This document is licensed under [CC-BY-ND-4.0](https://creativecommons.org/licenses/by-nd/4.0/legalcode) ([via SPDX](https://spdx.org/licenses/CC-BY-ND-4.0.html)).

The style of this documentation is adapted from [OpenAPI 3.0.2](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.2.md)
specification.

Fields that differ from the generic API, e.g. with stricter cardinalities, are highlighted in *italics*.

## Status of this Document

The current version of this document is `1.3.0`.

The version number will be applied according to [Semantic Versioning 2.0.0](https://semver.org/spec/v2.0.0.html).

## Definitions

### Cardinalities

| Cardinality | Description | REQUIRED |
|----|----|----|
| 1 | exactly one | yes |
| + | one or more | yes |
| ? | zero or one | no |
| * | zero or more | no |

### Data Types

| Type | Description |
|----|----|
| \[…\] | array |
| int | integer |
| string | a sequence of characters, MAY be empty |
| boolean | `true` or `false` |
| URL | a valid URL pointing to a resource |
| MIME type | a valid MIME type according to IANA. See [MDN](https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types) |
| semver | a string matching the pattern `^\d+\.\d+\.\d+$`, representing a semantic version number |
| iso639-3 | alpha-3 language code according to [ISO 639-3](https://iso639-3.sil.org/) |
| SPDX | a valid SPDX identifier (see <https://spdx.org/licenses/>) or – if not applicable, e.g. for copyrighted resources – `restricted` |

### URI Syntax

The URI Syntax is used according to the one described at [IIIF](https://iiif.io/api/image/2.1/#uri-syntax).

## Delivery Service

The TextAPI delivery service is the description of endpoints.
It is REQUIRED to use the `https` protocol.

### Collection

Returns a [Collection Object](#collection-object).

`collection` MUST be either `syriac` or `arabic-karshuni`.

#### Sample Call

`https://ahiqar.uni-goettingen.de/api/textapi/ahikar/{collection}/collection.json`

### Manifest

Returns a [Manifest Object](#manifest-object).

#### Sample Call

`https://ahiqar.uni-goettingen.de/api/textapi/ahikar/{collection}/{manifest}/manifest.json`

### Item

Returns an [Item Object](#item-object).

#### Sample Call

`https://ahiqar.uni-goettingen.de/api/textapi/ahikar/{collection}/{manifest}/{item}/latest/item.json`

## Schema

All fields that are not explicitly REQUIRED or described with MUST or SHALL
are considered OPTIONAL.
It is RECOMMENDED, however, to provide as much information as possible.

### Collection Object

A collection contains a curated list of texts.
It is served at the corresponding [endpoint](#collection).

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| @context | 1 | URL | the JSON-LD context of this object. MUST be `https://gitlab.gwdg.de/subugoe/emo/text-api/-/raw/main/jsonld/collection.jsonld` |
| textapi | 1 | semver | the TextAPI version covered by the implementation |
| x-app-version | 1 | semver | the current version of the Ahiqar back end application |
| id | 1 | URL | URL pointing to this collection |
| title | 1 | \[[Title Object](#title-object)\] | the title of the collection. the main title `MUST` be the first one in the array |
| collector | 1 | \[[Actor Object](#actor-object)\] | a personal entity responsible for the collection (collector) |
| description | *1* | string | description of the collection |
| sequence | 1 |\[[Sequence Object](#sequence-object)\] | a set of manifests included in this collection |
| annotationCollection | *1* | URL | URL pointing to an [Annotation Collection](https://www.w3.org/TR/annotation-model/#annotation-collection) for the complete collection |

### Sequence Object

Represents a sequence of collections, manifests or items.
Within a manifest it SHOULD contain items exclusively.

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| @context | 1 | URL | the JSON-LD context of this object. MUST be `https://gitlab.gwdg.de/subugoe/emo/text-api/-/raw/main/jsonld/sequence.jsonld` |
| id | 1 | URL | URL to find a [Manifest Object](#manifest-object), [Collection Object](#collection-object) or [Item Object](#item-object) |
| type | 1 | string | one of `collection`, `manifest`, `item` |
| label | *1* | string | human-readable name or title. `SHOULD` correspond to the collection's/manifest's/item's `label` or `title` property |

### Manifest Object

This is the main object in the schema to represent a single text, its derivatives
(e.g. HTML) and associated images.
It is REQUIRED to be served at the corresponding [endpoint](#manifest).

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| @context | 1 | URL | the JSON-LD context of this object. MUST be `https://gitlab.gwdg.de/subugoe/emo/text-api/-/raw/main/jsonld/manifest.jsonld` |
| textapi | 1 | semver | version number satisfied by the implementation |
| x-app-version | 1 | semver | the current version of the Ahiqar back end application |
| id | 1 | URL | URL pointing to this manifest |
| label | 1 | string | human-readable name or title |
| sequence | 1 | \[[Sequence Object](#sequence-object)\] | a sequence of `items` |
| metadata | *1* | \[[Metadata Object](#metadata-object)\] | contains the project specific info about editors, creation date/place and current location of a manuscript/book |
| support | *1* | \[[Support Object](#support-object)\] | additional files that help with the display of serializations |
| license | 1 | \[[License Object](#license-object)\] | license under which the resource MUST be used |
| annotationCollection | *1* | URL | URL pointing to an [Annotation Collection](https://www.w3.org/TR/annotation-model/#annotation-collection) for the complete manifest |

### Actor Object

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| @context | 1 | URL | the JSON-LD context of this object. MUST be `https://gitlab.gwdg.de/subugoe/emo/text-api/-/raw/main/jsonld/actor.jsonld` |
| role | 1 | [string] | the role of a personal entity in relation to the parent object, MUST be `collector` in case of collections |
| name | 1 | string | the principal name of the person |
| idref | ? | \[[Idref Object](#idref-object)\] | authority files related to the person |

### Metadata Object

A set of metadata describing the source or its context.
Mainly used for key-value pairs.

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| key | 1 | string | label |
| value | *1* | string | property |

### Image Object

An image representing the source or its provider.
The URL points to TextGrid's [IIIF Image API](https://iiif.io/api/image/2.0/) service which also allows for manipulations such as resizing.
The URL can have one of the following forms:

* `https://ahiqar.uni-goettingen.de/api/images/{image-uri}` for a complete page
* `https://ahiqar.uni-goettingen.de/api/images/{image-uri}/{x-offset},{y-offset},{width},{height}` for an image section described by `{x-offset}`, `{y-offset}`, `{width}`, and `{height}` as a sequence of percentages

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| @context | 1 | URL | the JSON-LD context of this object. MUST be `https://gitlab.gwdg.de/subugoe/emo/text-api/-/raw/main/jsonld/image.jsonld` |
| id | 1 | URL | URL pointing to the image |
| license | 1 | [License Object](#license-object) | the license for the image that MUST be used |

### License Object

The license or any other appropriate rights statement the resources is served under.
It is REQUIRED to use one of [SPDX](https://spdx.org/licenses/).

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| id | 1 | SPDX | an SPDX identifier |
| notes | ? | string | further notes concerning the license. can be used e.g. for the attribution statement of CC BY. `restricted` License Objects set the `notes` property and adhere to the [format of copyright notices](https://reuse.software/spec/#format-of-copyright-notices) suggested by the [REUSE Specification](https://reuse.software/spec) |

### Support Object

Any material supporting the view is described and referenced in this object.
This encompasses fonts and CSS, but also other material to support the rendering MAY
be added on request.

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| @context | 1 | URL | the JSON-LD context of this object. MUST be `https://gitlab.gwdg.de/subugoe/emo/text-api/-/raw/main/jsonld/support.jsonld` |
| type | 1 | string | MUST be either `font` or `css` |
| mime | 1 | MIME type | the MIME type for the resource |
| url | 1 | URL | URL pointing to the resource |

### Item Object

It is REQUIRED to be served at the corresponding [endpoint](#item).

The `type` MUST be `page`.

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| @context | 1 | URL | the JSON-LD context of this object. MUST be `https://gitlab.gwdg.de/subugoe/emo/text-api/-/raw/main/jsonld/item.jsonld` |
| textapi | 1 | semver | version number satisfied by the implementation |
| x-app-version | 1 | semver | the current version of the Ahiqar back end application |
| id | 1 | URL | URL pointing to this item |
| title | *1* | \[[Title Object](#title-object)\] | the title of the item |
| type | 1 | string | `page` |
| n | *1* | string | page number as given in `tei:pb/@n` |
| lang | 1 | [iso639-3] | language codes describing the resource |
| langAlt | ? | [string] | alternative language name or code (when there is no iso639-3 code, e.g. `karshuni`) |
| x-langString | 1 | string | a string listing all languages appearing in the document with their full English name (e.g. "Arabic, Karshuni") |
| content | 1 | \[[Content Object](#content-object)\] | different serializations of the item, e.g. HTML, plain text, XML, ... |
| image | ? | [Image Object](#image-object) | corresponding image as given in `tei:pb/@facs` |
| annotationCollection | *1* | URL | URL pointing to an [Annotation Collection](https://www.w3.org/TR/annotation-model/#annotation-collection) for this item |

### Content Object

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| @context | 1 | URL | the JSON-LD context of this object. MUST be `https://gitlab.gwdg.de/subugoe/emo/text-api/-/raw/main/jsonld/content.jsonld` |
| url | 1 | URL | URL pointing to the content |
| type | 1 | MIME type | a MIME type. If several Content Objects with the same MIME type are provided, these SHOULD be distinguished with a [MIME type parameter](https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types#structure_of_a_mime_type) where the key is `type` and the value is one of `text/html;type=transcription`, `text/html;type=transliteration` and `text/html;type=translation`. |

### Title Object

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| @context | 1 | URL | the JSON-LD context of this object. MUST be `https://gitlab.gwdg.de/subugoe/emo/text-api/-/raw/main/jsonld/title.jsonld` |
| title | 1 | string | a single title |
| type | 1 | string | one of `main`, `sub`. if several titles are provided, at least one of them MUST have the type `main` |

### Idref Object

| Field Name | Cardinality | Type | Description |
|----|----|----|----|
| @context | 1 | URL | the JSON-LD context of this object. MUST be `https://gitlab.gwdg.de/subugoe/emo/text-api/-/raw/main/jsonld/idref.jsonld` |
| base | ? | URL | the base URL to the authority file |
| type | 1 | string | short title of the referenced authority |
| id | 1 | string | the main ID referenced |

## Revision History / Changelog

| Version | Date | Description |
|---------|------|-------------|
| 1.0.0 | 2020-07-14 | initial version |
| 1.1.0 | 2020-08-25 | add more metadata for manifests/editions (`x-editor`, `x-date`, `x-origin`, `x-location`)|
| 1.1.1 | 2021-01-18 | update examples concerning Title Object |
| 1.2.0 | 2021-02-04 | move extensions to Metadata Object |
| 2.0.0 | 2021-02-18 | add Content Object for providing several serializations |
| 2.0.1 | 2021-03-09 | add missing docs for Support Object |

## Appendix

### Class Diagram

![UML class diagram](https://yuml.me/c099eddb.svg)

```yuml
// Ahiqar TextAPI
// ------------------

// classes
[Collection|entrypoint|-@context 1; -textapi 1; -x-app-version 1; - id 1; -title 1; -collector 1; -description 1; -sequence 1; -annotationCollection 1 {bg:yellow}]

[Manifest|entrypoint| -@context 1; -textapi 1; -x-app-version 1; -id 1; -label 1; -sequence 1; -metadata 1; -support 1; -license 1; -annotationCollection 1 {bg:yellow}]

[Item| -@context 1; -textapi 1; -x-app-version 1; -id 1; -title 1; -type 1; -n 1; -lang 1; -langAlt ?; -x-langString 1; -content 1; -image ?; -annotationCollection 1]

// objects
[Sequence| -@context 1; -id 1; -type 1; -label 1;]

[Actor| -@context 1; -role 1; -name 1; -idref ?]

[Image| -@context 1; -id 1; -license 1]

[License| -id 1; -notes ?]

[Title| -@context 1; -base ?; -title 1; -type 1]

[Idref| -@context 1; -base ?; -type 1; -id 1]

[Content| -@context 1; -url 1; -type 1]

[Support| -@context 1; -type 1; -mime 1; -url 1]

// imports
[Collection]-[Title]
[Collection]-[Actor]
[Collection]-[Sequence]

[Manifest]-[Sequence]
[Manifest]-[Metadata]
[Manifest]-[License]
[Manifest]-[Support]

[Item]-[Title]
[Item]-[Image]
[Item]-[Content]

[Actor]-[Idref]

[Image]-[License]
```

### Example Objects

* <https://ahiqar.uni-goettingen.de/api/textapi/ahikar/syriac/collection.json>
* <https://ahiqar.uni-goettingen.de/api/textapi/ahikar/arabic-karshuni/3r178/manifest.json>
* <https://ahiqar.uni-goettingen.de/api/textapi/ahikar/arabic-karshuni/3r7p1/1/latest/item.json>
