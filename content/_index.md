# Introduction

„Ahiqar“ (short for „The Syriac and Arabic manuscripts of the Ahiqar tradition“) is a
project that aims to create a new edition and analysis of the motifs of the Syriac and
Arabic manuscripts of the Ahiqar tradition.

## The Ahiqar TextAPI

In this project, we explore how we can create a generic [TextAPI](https://subugoe.pages.gwdg.de/emo/text-api/)
for the State and University Library and develop a specific derivate for an edition
project based on TEI.

The [Ahiqar TextAPI]({{< ref "text-api-specs" >}}) is fully compliant to the generic TextAPI and provides some addition as needed.
The terminology of the generic TextAPI applies as follows:

* **Collection**: In Ahiqar, refers to two levels: First it means the *main collection* of the Ahiqar project which contains the all manuscripts to be edited.
    Second, it refers to the *language collections*, `arabic-karshuni` and `syriac` with which the manuscripts are associated.
    While `syriac` corresponds to the Syriac language collection, `arabic-karshuni` combines the Arabic and the Karshuni language collection.
* **Manifest**: The term „Manifest“ refers to the TextGrid edition object which holds information about the associated XML document as well as the facsimiles.
* **Item**: „Items“ means pages within an edition in Ahiqar.
    These are encoded in `tei:pb/@n`.

## The Ahiqar AnnotationAPI

For providing annotations we created the [AnnotationAPI]({{< ref "annotation-api-specs" >}}) which is compliant to the [W3C Annotation Model](https://www.w3.org/TR/annotation-model/), but has several
additions to the W3C specification where needed.
These are marked with the prefix **x-**.

Its general design is congruent to the TextAPI, i.e. it assumes a threefold structure of Collections, Manifests, and Items.

**Annotation Collections** are defined for Collections, Manifests, and Items, whereas **Annotation Pages** are provided for Manifests and Items only.

## Interplay between TextAPI and AnnotationAPI

While the AnnotationAPI is agnostic of the TextAPI, the latter indicates annotations via the `annotationCollection` field in the respective Collection, Manifest, or Item Object.

![Interplay between TextAPI and AnnotationAPI](img/annotationAPI.png)

## Purpose of This Documentation

This document aims to describe in which way its TextAPI differs from the generic one
and how its AnnotationAPI is designed, hopefully serving as an example for other projects.
